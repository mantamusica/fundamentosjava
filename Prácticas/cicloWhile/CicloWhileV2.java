package cicloWhile;

import java.util.Scanner;

public class CicloWhileV2 {

    private static Scanner entradaEscaner;

	public static void main(String[] args) {
        System.out.println("Por favor introduce el número de elementos a iterar:");
        int maxElementos;
        entradaEscaner = new Scanner(System.in);
        maxElementos = entradaEscaner.nextInt(); //Leemos el valor proporcionado por el usuario
        int contador = 0;//Inicializamos el contador
        while (contador < maxElementos) {
            System.out.println("contador = " + contador);
            contador++;
        }
    }
}
